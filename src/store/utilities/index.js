import errors from './errors'
import success from './success'
import loading from './loading'
import locales from './locales'

export default {
    errors, loading, locales, success
}