export default [
  {
    title: 'Home',
    route: '/merchant',
    icon: 'bx bx-home',
  },
  {
    title: 'Chat',
    route: '/merchant/chat',
    icon: 'bx bx-chat',
  },
  {
    title: 'Produk',
    route: '/merchant/product-list',
    icon: 'bx bx-shopping-bag',
  },
  {
    title: 'Etalase',
    route: '/merchant/etalase',
    icon: 'bx bx-shopping-bag',
  },
  {
    title: 'Pesanan',
    route: '/merchant/transaction-list',
    icon: 'bx bx-clipboard',
  },
  {
    isDivider: true, 
  },
  // {
  //   title: 'Statistik',
  //   route: '/merchant/statistik',
  //   icon: 'bx bx-trending-up',
  // },
  // {
  //   title: 'Performa Toko',
  //   route: '/merchant/performance',
  //   icon: 'bx bxs-zap',
  // },
  {
    title: 'Dekorasi Toko',
    route: '/merchant/store-decoration',
    icon: 'bx bx-coffee',
  },
  {
    title: 'Ulasan',
    route: '/merchant/reviews',
    icon: 'bx bx-radio-circle',
  },
  {
    title: 'Pesanan Dikomplain',
    route: '/merchant/complaints',
    icon: 'bx bx-radio-circle',
  },
  {
    isDivider: true, 
  },
  // {
  //   title: 'Pusat Edukasi Seller',
  //   route: '/merchant/edukasi-seller',
  //   icon: 'bx bx-book',
  // },
  // {
  //   title: 'Bantuan MALMA',
  //   route: '/merchant/help',
  //   icon: 'bx bx-headphone',
  // }, 
  // {
  //   isDivider: true, 
  // },
  {
    title: 'Pengaturan',
    route: '/merchant/store-settings',
    icon: 'bx bx-cog',
  },  
]
